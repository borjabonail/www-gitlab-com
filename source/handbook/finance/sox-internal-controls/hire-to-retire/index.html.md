---
layout: handbook-page-toc
title: "Hire to Retire"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


**Introduction**

Hire to Retire includes the process of recruitment, payroll and separation from the organisation. This page decribes the various stages of the Hire to Retire process. 

This process is dependant upon the following applications:

 |Application| Primary use|
 |----|----|
 |Greenhouse| Recruitment process of candidates|
 |BambooHR| Maintains the personnel records of employees and pay structure of the employee|
 |ADP| Payroll processing of US entity employees|
 |Google Sheets|Payroll change report|


## 1.  Recruitment

[Link to flowchart](https://drive.google.com/open?id=1e9u9_0yD7Jedjj2iZwEqfk1s0EiJb8W3scrwJqHaE2U)

**Hiring in Greenhouse**


* GitLab uses Greenhouse for recruitment activities **(H2R.C.01 and H2R.C.03)**. Following are the key activities performed through Greenhouse:

    * Approving new jobs
    * Posting job vacancies internally
    * Reviewing Candidate applications 
    * Scheduling interviews
    * Approving offers **(H2R.C.01)**
    * Sending contracts of hire
    * Updating Job information <br><br>

Employee referrals are made through issues based on details mentioned [here](https://about.gitlab.com/handbook/hiring/referral-process/).

**Creating a job on Greenhouse**

* Navigate to the [Add icon](https://drive.google.com/open?id=17TUOnNCuRGXPStCqJZ2e71eee17uHFMNqoG9xfvtKto) in the upper right-hand corner and select Create a Job from the drop-down menu.

* If a job has been created in the past, a user with Job Admin level permissions on that particular job can elect to use that previous job as a template for the [new job created](https://drive.google.com/open?id=1SO708Hi0zUMUXPq8ApWmJkRhdcukkjnV77TOBMirBiM). To do so, click Copy an existing job. 

    * From the [subsequent page](https://drive.google.com/open?id=1K8eqlD2TuT0kNte0Q-YnybxREEt_Zt0UKRwtWKDAU3k), select either Copy an Existing Job.
    * Select a template, then proceed as per the Job setup flow configured in the system.
    * Complete the steps of the job setup flow to create the vacancy.
    * Once a vacancy has been selected to copy, the first screen will ask for basic job info.
    * Input the number of openings to be hired for this role.
    * Under "Employment Type", select if the vacancy is a full-time, part-time, intern, or contract role.
    * Under "Salary"
        * For roles in the [Compensation Calculator Tool](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/calculator/): Take the benchmark x level x 0.25 for the low end and benchmark x level x 0.85 on the high end.[Refer benchmark calculation](https://about.gitlab.com/handbook/people-operations/global-compensation/#exchange-rate)
        * For roles not in the compensation calculator (Sales/Director/Exec): Leave blank if not known. The People Operations Analyst will edit as the first level of approval. People Ops Analysts will pull survey data in San Francisco for this role then apply the same formula for 0.25 on the low end and 0.8 on the high end.
    * If the vacancy is eligible for a bonus, input the range of the bonus amount under the "Bonus" field. If there are no bonuses associated with this vacancy, then the field is to be left blank.
    * If this vacancy is eligible for stock options, input the range of offered stock options under the "Options" field. 
    * Under "Type" choose if this is a new hire or a backfill.
    * Click "Create Job & Continue".
    * The next page consists of all of the attributes interviewers will be evaluating for candidates in their scorecards across the full interview process.
    
**Scorecard updating**

* Scorecards are text boxes where the vacancy creator can add in his/her notes. Some scorecards for different roles or stages may have additional text boxes for specific questions. If a text box is required, there will be a red asterisk by the question.
* Underneath the first text box "Key Take-Away".
* Further, there are two additional links, Private Note and Note for Other Interviewers, which will open upon clicking.
* Below the text boxes for notes, each role has a list of attributes that we are looking for. Each stage in the process has certain attributes highlighted that it's recommended for evaluation.
* The scorecard will automatically save the entered information.
* Finally, click Submit Scorecard. 

**Creation of stages in candidate assessment**

* The next section is the Interview Plan, fill in the Interview Plan accordingly.
* The Application Review stage is mandatory.
* Many vacancies have an Assessment as the first step in the process.
* If the vacancy requires an Assessment, but there is no Assessment stage already added, scroll to the bottom of the page and click ["Add a Stage"](https://drive.google.com/open?id=1YNN-Im-QbxK75v-eB7wjou6WmCbzM_r7ZV2QkfWgEjM).
* Click the “Edit” button, then create the stage by filling in the applicable fields, such as “Custom Questions.” In the “[Interview Prep](https://drive.google.com/open?id=1j5W6wvw2mY7nOke1wsq9pCkk91DGo3gpFbfuc5smqRA)” section, specify the items the graders have to look for when they review the candidates' answers.
* Finally, under "Additional Settings", check "This interview requires scorecards to be submitted" and leave unchecked "Hide candidate name and details from grader."
* Click "Save".
* The next stage is the screening call stage, which should be standard across the board. It is recommended to click "Edit" on this stage, scroll to the bottom, and choose the recruiter as the default interviewer. 
* The next stage is the team interview, where the candidates will meet with peers and the hiring manager.
* Under this stage, there will be different types of interviews. They are typically called "Manager Interview", "Peer Interview 1", and “Peer Interview 2". 
* For each interview in the stage, click "Edit" next to it. First, select the appropriate attributes to focus on in that interview. Then include the purpose of the call and sample questions the interviewer should ask.
* The two "Additional Settings" should both be checked.
* Then click "Save".
* The next stage is the executive interview stage, where there are two interviews with the executive for the division of the vacancy as well as the optional CEO interview. The executive interview can be customized as needed following the guidelines for the team interview, including selecting attributes, adding custom questions, and selecting a default interviewer. 
* The next stage is for reference checks, with two sections for a former manager and a former peer of the candidate. These can be customized as needed.
* The last stage is the offer stage. (Please refer creating a vacancy or offer approval flow)

**Assignment of hiring team**

* The next section is assigning the hiring team & the related access restrictions
* The first step is to scroll down to the "Who Can See This Job" to assign permissions to the team members who will need access. Continue scrolling to "Job Admin: Hiring Manager" and click the pencil and add the Hiring Manager(s), including their Managers, Directors, and Executive, then click save.
* Scroll back to the top of the page and select the main people responsible for the job. Under "Hiring Managers", click the pencil and select the Hiring Manager for the vacancy and click save. Under "Recruiters", select the recruiter assigned to the particular division. Under "Coordinators", select the coordinators assigned to the  division. 
* Once the hiring team is added, click "This looks good, NEXT" at the right.
* The next section is the Approvals section. (Please refer creating a vacancy or offer approval flow)
* Double check that the vacancy posted correctly in Greenhouse.

**Creating a vacancy or offer approval flow (If unavailable)**

* Log in to Greenhouse and go to the configure section by clicking the gear at the top right corner.
* Then click "Approvals", where there will be two columns (job approvals on the left and offer approvals on the right) separated out by divisions/departments. Each division/department that either has a unique division in Greenhouse OR a unique executive should have its own section.
* Scroll to the bottom of the page and click "Add Approval by office/department".
* Then choose what division/department. Do not select an office. Click "Create".
* Under both the job and offer approvals columns for the new section, if there are any automatic additions that populate, remove them by hovering over them and clicking the "x".
* Click "Add Approval Step" under the jobs approval column. Then click "Add" and search for the appropriate team member for Step 1 and select their name. If there should be more than one team member, click "Add" again and search for the team member’s name and select their name.<br> 
  Note #1: The vacancy approval flow should follow the below mentioned approval flow format.
* Then, Click "Save".
* Perform the same process to update offer approval flows.<br>
  Note #2: The offer approval flow should follow the below mentioned approval flow.

    |Job approvals flow (Note #1)|
    |----|
    |Step 1: People Ops Analyst (only 1 required)|
    |Step 2: FP&A (only 1 required)|
    |Step 3: Executive of division/department (only 1 required if multiple people are listed)|
    |Step 4: FP&A (only 1 required) - 2nd stage approval or Official Job approval|

    |Job offers flow format (Note #2)|
    |----|
    |Step 1: People Ops Analyst (only 1 required)|
    |Step 2: FP&A (only 1 required)|
    |Step 3: Executive of division/department (only 1 required if multiple people are listed)|
    |Step 4: CPO and CEO (only 1 required) for Director-level roles and above|

**Changing an existing section of approvals**

* If the participants in an approval step are to be changed, click the pencil, then it can be removed and/or added team members.
* If an approval step is to be deleted, then delete an approval step in its entirety, click the "x".
* If the approval order is to be changed, then click and drag that step to the appropriate placing.

**Publicizing the vacancy**

* The careers page currently links to a general 'Talent Community' submission, which is not tied to any jobs.  
* Team Members submit their referrals via an Issue, which mirrors the 'Referral In-Take Questionnaire.' Details about this process can be seen [here](https://about.gitlab.com/handbook/hiring/referral-process/)
* The hiring team may advertise the vacancy through the following sites and is open to posting to more, in order to widely publish a variety of vacancies.

    |Sl.no|Particulars|
    |---|----|
    |1|Alumni Post|
    |2|LinkedIn|
    |3|Remote Base|
    |4|WeWorkRemotely|
    |5|RemoteOK|
    |6|Indeed Prime|
    |7|Ruby Weekly|
    |8|Glassdoor|
    |9|AngelList|

* The hiring team can create specific tracking links through Greenhouse in order to include the specific source of different job boards.


**Creating tracking links in Greenhouse for external postings**


* Click the [Configure icon](https://docs.google.com/document/d/1Fxpdg3S_5q7J5JqTR7JdVJ0Cc6ijLP0vRK7ilh6pU7c/edit) in the upper-right hand corner and navigate to Job Board on the left-hand panel.
* Select a job board from the subsequent list and click the ellipsis inline with the job board. Click [Tracking Link](https://docs.google.com/document/d/1e8yL2M0Sc1_ixTbwpEamn_d70xXd26fykf4nwy1McB8/edit) from the dropdown menu.
* Use the [Get a Tracking Link dialog box](https://docs.google.com/document/d/14Jn5rNltJzTgVVKjR05UEmAyyhekVc4La_pRiwGxdIQ/edit) to configure the Who gets credit and Source fields. Click Create Link when finished.
    * A tracking link for the job board will be generated in the provided field. Copy the tracking link and share.
 
**Offer Packages**

After completion of the interview and candidate selection, approval in Greenhouse needs to be initiated.

**Offer Package Creation**

* The hiring manager will work with the recruiter on the offer details and the recruiter will be responsible for submitting the official offer details through Greenhouse.
* To create the offer package, move the candidate to the "Offer" stage in Greenhouse and select "Manage Offer." Input all required and relevant information, ensuring its correctness, and submit; then click Request Approval **(H2R.C.02)**. 
* Information in the offer package for counter offers should include the following in the "Approval Notes" section:
    
    * New offer
    * Original offer
    * Candidate's salary expectation beginning of process
    * Candidate's counter offer<br><br>

* Anyone making comments regarding an offer should make sure to @mention the recruiter and hiring manager:
    
    1. The People Ops Analyst will receive an email notifying them of the offer.
    1. The People Ops Analyst will ensure the compensation is in line with compensation benchmarks.
    1. Only one approval is needed in order to move forward.
    1. If the hire is not in a low location factor area above 0.9, the e-group member responsible for the function and the CFO will be notified.
    1. The FP&A will receive a notification to approve.
    1. The executive of the division will then receive a notification to approve.
    1. Lastly, for Director-level roles and above, the CEO and CPO will receive a notification to approve.
    1. Only one approval is required in order to move forward with the offer.
    1. Typically, the CPO will provide the final approval, but if the CPO is out of office, the CEO will be the final approver.

* It is recommended to also ping approvers, especially the executive (and CEO if needed) in Slack with the message 


**Template for Message**

"Hiring approval needed for [Candidate Name] for [Position]" with a link to the candidate profile. 

To create the link, search for the candidate in Greenhouse, select the candidate, go to their offer details page, and copy the link. Do not copy a link from a different section of their candidate profile.


**Final offer approval**

For pending offer approvals needed from the CPO/CEO, there is an #offersSlack channel where the requests should be added. 

Please ensure the ping has:

 1. Name
 1. Position
 1. For re-approvals clearly indicate what changed and why

If the role is for an individual contributor, the CPO or CEO do not need to approve. However, please @mention the CEO in the '#offers' channel with "Offer has been extended for [Candidate Name] for [Position]" and a link to the candidates Greenhouse profile.


**Communicating the Offer**

Once the offer package has been approved by the approval chain, the verbal offer will be given, which will be followed by an official contract, which is sent through Greenhouse.

Offers made to new team members should be documented in Greenhouse through the notes thread between the person authorized to make the offer and the Candidate Experience Specialist.

Greenhouse offer details should be updated as necessary to reflect any changes including start date. Sections updated that will trigger re-approval are noted in Greenhouse.


**Employment Contracts**

* One person from the recruiting team (typically the Candidate Experience Specialists) will prepare the contract. Following are the steps for preparation of employment contract:

    1. Check all aspects of the offer.
    1. Generate the contract within Greenhouse using a template based on the details found in the offer package.
    1. Contact the recruiter or new team member to gather any missing pieces of information (note: the address can be found on the background check information page).
    1. Ensure that, if the contract was created outside of Greenhouse, the contract has been reviewed and approved by the Senior Director of Legal Affairs or a People Operations Analyst.
    1. Stage the contract in DocuSign from within Greenhouse, which emails the contract to the signing parties, with the recruiter, recruiting manager, and the hiring manager cc'd. One of the Recruiting Managers will sign all contracts before they go to the candidate for signatures. If the Recruiting Managers are out of office, the VP of Recruiting will sign.
    1. Enter the new team member's details on the Google sheet GitLab Onboarding Tracker and continually update it during the offer process.
    1. When the contract is signed by all parties, the Candidate Experience Specialist verifies that the start date in Greenhouse is correct; then they will mark the candidate in Greenhouse as "Hired" and export to BambooHR (do not export to BambooHR if it is an internal hire). 
    1. The Candidate Experience Specialist will upload the signed contract and the completed background check into the BambooHR profile.
    1. The Candidate Experience Specialist will email the new team member the Welcome Email from Greenhouse with a Cc to the recruiter, IT Ops and hiring manager.
    1. Instructions on the Notebook Ordering Process are included with this email.
    1. The recruiter will remove the vacancy in Greenhouse and dispose any remaining candidate profiles if necessary. Once complete, the recruiter will ping the Candidate Experience Specialist to close the role or close the role themselves.
    1. The final step before handing off to the People Operations Specialists is for the Candidate Experience Specialist to ensure all fields for the new team member on the GitLab Onboarding Tracker are updated and complete through the "GitLab Welcome Email Sent" column. 


**Onboarding**

On completion of recruitment and release of offer, People Operations Specialist uses GitLab internal issue tracking tool to create an Onboarding Issue. 

Onboarding issue keeps track of all the tasks to be completed before onboarding a candidate. People Operations Specialist assigns tasks to various team members on the [onboarding issue.](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md)

**Adding a New Team Member to BambooHR**

As part of onboarding, People Operations Specialists will process new hires in BambooHR. Following are the activities performed by People Operations Specialists in BambooHR to update new hires.

* Effective Date - Hire Date
* Location - Which entity the new team member is contracted through.
* Division - Enter the appropriate division from the dropdown.
* Department - Enter the appropriate department from the dropdown.
* Job Title - Choose the job title. If it does not exist already, scroll to the bottom, click "Add New" and save the new job title.
* Reports To - Select their manager.
* Enter whether the team member is part-time or full-time. Any comments add them to the compensation table.
* Employment Status
* Enter the hire date and set the status to active. Also leave a comment if there is anything of note in the contract.
* Pay Rate - Entered as if it were a payroll amount. For example, a US employee would be entered as their yearly amount divided by 24 payrolls in a year. A contractor would have their monthly contract amount listed.
* Pay Per - Monthly for contractors and employees paid once per month, Pay Period for all other employees
* Pay Type - Use either Salary or Hourly for employees, or Contract for contractors.
* Pay Schedule - Select the pay period. Currently we have twice a month for the US, and monthly for all others.
* [Pay Frequency](https://about.gitlab.com/handbook/contracts/#employee-types-at-gitlab):
    * 12.96 For GitLab B.V. employees in the Netherlands
    * 13.92 for GitLab B.V. employees in Belgium
    * 24 for GitLab Inc. employees in the United States
    * 12 for everyone else paid monthly


**Auditing Bamboo HR entries:**

At the end of each week a People Operations Analyst will review all data entered into BambooHR through the Payroll Change Report for audit purposes. Once a month an audit is conducted from all payroll providers to ensure the salary information matches BambooHR.



## 2. Employee Master Creation and updates

[Link to flowchart](https://drive.google.com/open?id=1TP-cxXdfDhZ0bhZ3Gvb1qWrl6exuU2U5V08oVLOyU_0)

GitLab uses ADP for payroll processing. Employee masters are created in ADP by the Payroll team before payroll processing based on inputs from Bamboo HR **(H2R.C.10, H2R.C.11, H2R.C.12, H2R.C.14 and H2R.C.15)**.

**Creation of Employee in ADP**

Employee profiles are created in ADP 

The following steps are followed to create an employee's profile on ADP **(H2R.C.04, H2R.C.05, H2R.C.06, and H2R.C.07)**

* BambooHR is integrated with ADP and employee profiles are created in ADP automatically.
* Employee profiles in ADP are created only after completing the [I9 verification on BambooHR](https://docs.google.com/document/d/1uhkqq7a6dtcsJrAek9lHZxxhOjRJZCTXRpH5okLMqkQ/edit?usp=sharing). 
* On completion of the I9 verification, the “I-9 processed” checkbox is checked in employee’s profile on BambooHR. An [automated batch job](https://docs.google.com/document/d/1XwzVrI-GEd87JpNZyIdsfcWwZmP5C6IVgy9wj1ro4Qs/edit?usp=sharing) that runs daily between 7:00 AM and 7:15 AM (CST)  by modulus picks up new I9 verified employee profiles from BambooHR and loads it into ADP.
* Go to onboarding issue and check off [payroll task](https://docs.google.com/document/d/1dM8cZZMYI88s_6T-SfawFX9mRDWGIVZIN8kjlVewjFY/edit) 
* [Email team member](https://docs.google.com/document/d/1KVCc3ZDqlt7yaGz4d7J0gEIyfgX9omazP0C60U18dWE/edit) with information about completing registration
* [New hire report from BambooHR](https://docs.google.com/document/d/14Vsxkw9kkVSxasiisFoUCp7RI5z4mn5ZxTqFZNp6A44/edit) for list of team members by start date, mark with “x” once complete.

**Employee Terminations**

Employee Terminations are updated on ADP in the following way:

* In [ADP](https://docs.google.com/document/d/1sha3hvrHj85IFYPvFEDckD1udfbTwr2OMVkHnrTLzvc/edit), Navigate to  people > Employment> Employment profile
* [Search Team Member](https://docs.google.com/document/d/1WzXw7QIxKJIPgOOS_pweVYCqJiKCRUfoi26G4k0NHkA/edit)
* Update the [employee status](https://docs.google.com/document/d/18JCaxTOvABEP7uiBrfxTswqQ-aqltmwmJloRYDGFgDw/edit)
* Start [Termination Wizard](https://docs.google.com/document/d/1ScXsbvREHsCBdSCKjwG9F9999TLzv0ju1zx-Lr6sWNw/edit)
* Enter [term date](https://docs.google.com/document/d/11j1nXkcUDL7uwHOobUA0fc3-bjAEbqqIQJR_DCHgDgw/edit), term reason (confirm with HR) and choose voluntary or involuntary. If voluntary, keep eligible for rehire checked.
* Next, make sure autopay is canceled, do not cancel direct deposit in case of a future commission payment and update in [ADP](https://docs.google.com/document/d/1wXHTjNW21gUG3Hh0bzOYIwTTzpR51nhPEQgE1WrFneI/edit).
* Comment on the [off-boarding issue](https://docs.google.com/document/d/1ABhpmORr88gVUDHArxrGMLpsmh7c_38cnwgp66yykPg/edit) once complete in ADP.


On every paycheck date, an audit of all entity payroll providers is conducted to ensure the salary information matches BambooHR. This is done by payroll specialist/payroll specialist EMEA and approved by the Senior Manager, Global Payroll and Payments.

**Process for Non-US Entities**

For the following countries, the Payroll Specialist will extract the payroll data and send to the payroll provider, who will in turn add the new employees for the respective countries

 * Canada
 * Australia
 * Germany

**Process for PEOs**

For PEOs, the new employee information to be added is sent by the recruitment team to the PEOs. 



## 3. Payroll Processing for US

## 4. Payroll processing for Non-US

## 5. Leave Management for payroll processing (LOA, leave of absence)

## 6. Leave Management 



