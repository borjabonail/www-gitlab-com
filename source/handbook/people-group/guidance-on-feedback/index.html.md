---
layout: handbook-page-toc
title: Guidance on Feedback
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Types of Feedback

At GitLab, as part of the written performance feedback, we use [360 Feedback](/handbook/people-group/360-feedback/). 

Performance Feedback should also be given at the 1-1s and you can find details about that by going to the [1-1 page](/handbook/leadership/1-1/).

Additionally there is 365 feedback. Feedback should be given 365 days a year and largely it’s done verbally and directly; not escalated. More than just for feedback, these concepts are used in any difficult conversation.

## Feedback can feel a little bit like this

![megaphone](/images/Training/Megaphone2.png)

## Importance of Feedback 

* Keeps everyone on track 
* Helps avoid major mistakes 
* Form better relationships 
* Motivates people
* Promotes personal & professional growth by establishing a Growth Mindset
* Enables a friendly work environment 
* Enables leaders to set the standard
* Instills trust amongst the team

## Fear and Holding Back

Giving feedback can be a scary process which makes it hard to do. This is because there are fears of damaging the relationship, being wrong, losing face or hurting the person. Holding back on providing feedback because you feel it isn't your place (if you are a peer) or believing it won't make a difference are also some reasons why we hold back.  

The consequences of holding back can have a significant impact to GitLab's culture. Patrick Lencioni, in his book [The Five Dysfunctions of a Team (2002)](https://www.tablegroup.com/books/dysfunctions) cites the following five consequences:

- Inattention to Results
- Avoidance of Accountability
- Lack of Commitment
- Fear of Conflict
- Absence of Trust

## Elements of Getting it Right

![Feedback Image](/images/Training/Feedback_Image.png)


## Radical Candor™

"Radical Candor™ just means Care Personally AND Challenge Directly.
Why does something so simple feel radical?" [Source](https://www.radicalcandor.com/).
There is a video below from Kim Malone Scott titled **Radical Candor-The Surprising Secret to being a good Boss** which explains this crucial element of getting feedback right.

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/4yODalLQ2lM" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## What comes first, Honesty or Trust?

For many, it is more comfortable to give feedback to, and receive feedback from, those with whom you have already established trust. Somehow, the trust makes it easier to assume good intent and to be boldly honest with each other.  Often, however, we need to provide feedback benefiting from trust before that trust has been earned.  
- **Task based trust before relationship/behavior based trust**:  While earning trust, it is more comfortable to give and receive task based feedback. It is easier to tell someone that there is a typo in something they have written than to tell someone their leadership is ineffective because they micromanage, for example. However, it can be surprising to realize how giving feedback can build trust. Sometimes, starting with the task based feedback can lead to more transformational behavior feedback.
- **Building Trust**:  Building trust on teams can be difficult but deserves attention and focus. Many of the suggestions in the handbook around team communication help with this. As a team, reading [The Advantage](https://www.amazon.com/Advantage-Enhanced-Organizational-Everything-Business-ebook/dp/B006ORWT3Y)  will provide you with some helpful hints for discussions you can have together to build trust.
- **Candidness Builds Trust**: Believe it or not, being candid with someone can actually build trust. Don't you get a little frustrated when returning home from a social engagement only to realize that you have a strawberry seed in your teeth.  Why didn't anyone tell you?  The same is true at work. When someone is willing to give you feedback that helps you be more effective, you can begin to trust that person more. You know they are willing to be honest with you, even when there is no direct advantage to them for doing so. Most would certainly trust that person more than the person who says it behind their back instead.  Let's be honest with each other.


## Cross-Cultural Feedback Considerations

GitLab has team members from many different cultures and backgrounds. Everyone responds to things differently. You may need to adapt your tone and style according to the individual and the relationship you have with them. Some things to think about are:

- Do you need to build relationship before candor is comfortable?
- Consider comfort with direct vs indirect feedback
- Is their communication generally explicit or implicit?
- Influence of hierarchy in direct feedback?
- What are the [cultural dimensions](https://www.hofstede-insights.com/models/national-culture/) in the feedback recipient's culture? You can overview all six cultural dimensions in [this tool](https://www.hofstede-insights.com/country-comparison/) for many, many countries all around the world to ensure you are as prepared as possible prior to providing feedback. 

## Hard Conversations

It is inevitable that at some point difficult feedback will need to be given. This type of feedback is actually extremely valuable if delivered correctly. Another important factor is to consider the individual and be prepared for how they might react. You may receive one or a combination of the following responses:

- Attack Mode: defensiveness, anger
- Silent Fuming
- Speaking openly, honestly and effectively

The last point is what we want everyone to be able to do. The best way to ensure you deliver feedback is to be prepared. You can do this by asking yourself some questions beforehand. These will help you to balance heart and mind, such as:

- What do I really want for myself?
- What do I really want for the other(s)?
- What do I really want for the relationship?
- How would I behave if I really wanted these results?

## Delivering Feedback

### Preparing to Give Candid Feedback

Consider:
- **Root Cause**: What are the behaviors I’ve experienced? What is the core issue? What would improve the relationship between you and the other person/ team?
- **Impact**: What is the effect/impact of the behaviors on you/others (positive or negative)? How is the behavior not aligned with GitLab's values? Cost/benefits to the person or team?
- **Suggestions/Actions**: What do you want the person(s) to do differently; what actions could they take?
- **Communication Styles/Culture Map**:  Could the individual’s background affect the way they prefer to receive feedback? Reading The [Culture Map](https://erinmeyer.com/book/) can help you understand the communication and feedback styles of different countries. Something very useful at GitLab.

### Guidelines for Delivering Feedback 

1. Have positive intent 
   * If you have the intent to hurt someone and not to help them, reconsider giving the feedback. 
   * Harsh feedback does not help people thrive and excel. 
1. Use Radical Candor™
   * Care Personally AND Challenge Directly 
1. Make it a frequent event and do it in real time
   * Don’t wait until performance reviews to deliver 
1. Avoid using absolutes 
   * For Example: "You always." "You never show up to meetings on time.") 
1. Feedback should be both positive and constructive 
   * It is recommended to use a 4:1 ratio of positive to constructive 
1. Use the Situation-Behavior-Impact (S-B-I) Model
1. Allow the feedback receiver time to reflect 


### S-B-I Model

The Situation-Behavior-Impact (S-B-I) Model helps structure feedback in a manner that makes it easily understandable.

**Situation** - Define the when and where by anchoring in time and place.

**Behavior** -  Describe the observable behavior and how it was applied.

**Impact** - Describe how the other person’s action affected you or others experiences and thinking.

### Documenting Feedback 

**Why it's important to document:**

* Ensures both giver and receiver are on the same page; No Surprises! 
* Provides focus and helps track progress
* Lays the ground for self reflections and performance discussions
* Supports decision making

**Where to document:**

* In 1:1 meeting agendas
* Culture Amp
* Performance evaluation tool
* Thanks Slack channel

### Live Learning Session on Delivering Feedback 

On 2020-06-08 we held three Live Learning sessions to cover how to deliver feedback effectively using the guidelines above. This recording is from the second session and includes content as well as a Q&A portion. The content in the video below follows along with this [slide deck](https://docs.google.com/presentation/d/1Wz8hr98CTiaytz1yIxG-D7H1P1eCm7ejnUl8O9YANEU/edit?usp=sharing) and [meeting agenda](https://docs.google.com/document/d/1wpS1kfRitFuBXihU-Z0fRkzEbtN1mx5z6bpJlqb1N1Y/edit?usp=sharing). We also used Mentimeter during the sessions to ask the attendees questions. Team members can view the [Mentimeter results](https://docs.google.com/spreadsheets/d/1gpxH80LNbXRUQdb9fFxlFDB-1QuGeO4D0XmaNNly1Fw/edit?usp=sharing). 

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/KSp4jIN2W5E" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Receiving Feedback

Receiving feedback well is an important skill to have not just at work, but in life in general. Receiving constructive and even positive feedback can be difficult. Our brains want to protect us from any potential dangers, and receiving feedback can be perceived by the brain as a physical threat. We have outlined some guidelines and tips to help with this. 

### Guidelines for Receiving Feedback

1. Assume positive intent 
   * This is likely uncomfortable for the feedback giver as well 
1. Be an active listener 
   * This is hard because our brains want us to run away 
   * Slow down and really take the time to listen to what the other person is saying so you can reflect on it 
1. Be respectful
   * Give them your full attention 
1. Ask questions 
   * It’s okay to take time to reflect on it and then come back with questions at a later time 
1. Show appreciation
   * Say thank you and mean it 
1. Reflect on the feedback
   * Take time to reflect on what you have heard and then think about the action 
   * Pitfall: Over-engineering or overdoing the response
1. Make a decision 
   * What are the most impactful actions you can take - prioritize
   * Who has what responsibility in follow up (i.e. giver/receiver)? Both.

### Live Learning Session on Receiving Feedback 

On 2020-02-25 we held three Live Learning sessions to cover how to receive feedback effectively using the guidelines above. This recording is from the first session and includes content as well as a Q&A portion. The content follows along with this [slide deck](https://docs.google.com/presentation/d/1yziTxwnAHD6vA1deSPf4DCwGLNYRijAmHiYcdoAxOmo/edit?usp=sharing), and the Q&A follows along with this [meeting agenda](https://docs.google.com/document/d/1k5ja-8qJRMnguVOUOOsMUAQ2PEbXiuDBsbpstJPkSEo/edit?usp=sharing).

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/ZdUMoI1jMvI" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Further Information

- [Your Employees Want the Negative Feedback you hate to Give](https://hbr.org/2014/01/your-employees-want-the-negative-feedback-you-hate-to-give)
- [Principles of Charity](https://en.wikipedia.org/wiki/Principle_of_charity)
- [Difficult Conversations: How to Discuss What Matters Most](https://www.goodreads.com/book/show/774088.Difficult_Conversations)
- [The Culture Map](https://www.amazon.com/Culture-Map-Breaking-Invisible-Boundaries/dp/1610392507)
- On 2018-02-15, we recorded a training on [Peer to Peer Feedback](https://www.youtube.com/watch?v=yzjAeu7RpU8&feature=emb_logo)

## Helpful Tools
- [Hofstede Country Comparison Tool](https://www.hofstede-insights.com/country-comparison/)
