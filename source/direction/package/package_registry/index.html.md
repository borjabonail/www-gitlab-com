---
layout: markdown_page
title: "Category Direction - Package Registry"
---

- TOC
{:toc}

## Package Registry

Our goal is for you to rely on GitLab as a universal package manager, so that you can reduce costs and drive operational efficiencies. The backbone of this category is your ability to easily publish and install packages, no matter where they are hosted. 

You can view the list of supported and planned formats in our documentation [here](https://docs.gitlab.com/ee/user/packages/).

- [Maturity Plan](#maturity-plan)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Category%3APackage+Registry)
- [Overall Vision](https://about.gitlab.com/direction/ops/#package)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)

This page is maintained by the Product Manager for Package, Tim Rizzi ([E-mail](mailto:trizzi@gitlab.com))

## What's Next & Why

We are currently focused on two epics for the GitLab Package Registry.

The first, [gitlab-#2614](https://gitlab.com/groups/gitlab-org/-/epics/2614), captures our plan to improve our existing integrations by expanding our existing product to include the creation, management and usage of **remote** and **virtual** repositories.  
- [gitlab-#9164](https://gitlab.com/gitlab-org/gitlab/-/issues/9164) investigates how to implement the above.
- [gitlab-#212168](https://gitlab.com/gitlab-org/gitlab/-/issues/212168) will design the user interface and experience.

The second, [gitlab-#2867](https://gitlab.com/groups/gitlab-org/-/epics/2867) will move the Package Registry to GitLab Core, so you can rely on GitLab for all of your Package management needs, regardless of your pricing tier.

## Maturity Plan

This category is currently at the "Viable" maturity level, and
our next maturity target is "Complete" (see our [definitions of maturity levels](/direction/maturity/)).

For a list of key deliverables and expected outcomes, check out the epic, [gitlab-#2891](https://gitlab.com/groups/gitlab-org/-/epics/2891), which includes links and expected timing for each issue.

## Competitive Landscape

|         | GitLab | Artifactory | Nexus | GitHub | JetBrains
| ------- | ------ | ----------- | ----- | ------ | ------ |
| Composer    | ✔️ | ✔️ | ✔️️️️ | - | - |
| Conan       | ✔️ | ✔️ | ☑️ | - | - |
| Debian      | - | ✔️ | ✔️ | - | - |
| Maven       | ✔️ | ✔️ | ✔️ | ️✔️ ️| ✔️ |
| NPM         | ✔️ | ✔️ | ✔️ | ✔️ | - |
| NuGet       | ✔️ | ✔️ | ✔️ | ✔️ | - |
| PyPI        | ✔️ | ✔️ | ✔️ | - | - |
| RPM         | - | ✔️ | ✔️ | - | - |
| RubyGems    | - | ✔️ | ✔️ | ✔️ | - |


☑️ _indicates support is through community plugin or beta feature_

## Top Customer Success/Sales Issue(s)

- Our top customer success and sales issues are all focused on adding support for new package manager formats:
  - [gitlab-#803](https://gitlab.com/gitlab-org/gitlab/issues/803): RubyGems
  - [gitlab-#5835](https://gitlab.com/gitlab-org/gitlab/issues/5835): Debian
  - [gitlab-#5932](https://gitlab.com/gitlab-org/gitlab/issues/5932): RPM

## Top Customer Issue(s)

- Our top customer issues are [gitlab-#211926](https://gitlab.com/gitlab-org/gitlab/-/issues/211926) and [gitlab-#212209](https://gitlab.com/gitlab-org/gitlab/-/issues/212209) which resolve issues with downlading NuGet packages and dependencies. 


## Top Internal Customer Issue(s)

- The Distribution team utilizes an external tool for building Linux packages. By offering support for Debian and RPM we can begin to remove that external dependency.
- The GitLab Distribution team also utilizes RubyGems for downloading external dependencies. They would like to speed up their build times and remove their reliance on external dependencies by caching frequently used packages. This would require an integration with RubyGems as well as the dependency proxy. [gitlab-#225](https://gitlab.com/gitlab-org/distribution/team-tasks/issues/225) details their needs and requirements.
- The Release group would like to leverage the GitLab Package Registry to store release assets and make them available for discovery and download. Although we are still working through how to best implement this, [gitlab-#36133](https://gitlab.com/gitlab-org/gitlab/issues/36133) discusses the need and use cases.

## Top Vision Item(s)

Our top vision item is [gitlab-#2614](https://gitlab.com/groups/gitlab-org/-/epics/2614) which will add support for remote and virtual repositories.

