require 'yaml'

REQUIRED_KEYS = %w[factor grade title type].freeze

def check_file
  role_levels = YAML.load_file(File.expand_path('../../data/role_levels.yml', __dir__))

  # check if there's always a `is_default` set
  # check if for all the levels the required keys are set
  role_levels.each do |role_title, levels|
    send_default_missing_error(role_title) if levels.none? { |level| level['is_default'] }
    send_value_is_missing_error(role_title) if levels.any? { |level| !level_has_required_keys?(level) }
  end

  # check if the job_families always refer to an existing level
  job_families = YAML.load_file(File.expand_path('../../data/job_families.yml', __dir__))
  all_levels = role_levels.keys
  job_families.each do |job_family|
    send_level_is_missing_error(job_family) unless all_levels.include? job_family['levels']
  end
end

def level_has_required_keys?(level)
  level.delete('is_default')
  level.keys.sort == REQUIRED_KEYS.sort
end

def send_default_missing_error(role_title)
  raise "In role_levels.yml: `is_default` is missing for #{role_title}."
end

def send_value_is_missing_error(role_title)
  raise "In role_levels.yml: not all fields are set for #{role_title}."
end

def send_level_is_missing_error(job_family)
  raise "In job_families.yml: the level #{job_family['levels']} for #{job_family['title']} does not exist in `role_levels.yml`."
end

p "Checking role_levels.yml and job_families file because changes have been detected to one or both of those files."
check_file
p "Done checking role_levels.yml file"
