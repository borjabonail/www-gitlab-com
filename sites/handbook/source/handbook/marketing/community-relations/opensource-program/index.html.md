---
layout: handbook-page-toc
title: "Open Source Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## GitLab for Open Source Program Overview
*At GitLab, our mission is to change all creative work from read-only to read-write so that everyone can contribute.* 

The [GitLab for Open Source](/solutions/open-source/) program supports GitLab's mission by empowering and enabling open source projects with our most advanced features so that they can create greater impact and amplify the contribution mindset within their spheres of influence.

This program's vision is to make GitLab the best product for Open Source projects to thrive at scale. 

Apart from actively maintaining and constantly adding value to the open source [Community Edition](/install/ce-or-ee/), GitLab makes the Enterprise Edition's top [Ultimate](/pricing/#self-managed) and [Gold](/pricing/#gitlab-com) tiers available free-of-cost to qualifying open source projects. 

### What's included?
- GitLab's top tiers ([self-hosted Ultimate and cloud-hosted Gold](/pricing/)) are [free for open source projects](/solutions/open-source/)
- 50,000 CI minutes are included for free for GitLab Gold users. Additional CI minutes can be purchased ($8 one-time fee for 1,000 extra CI minutes)
- Support can be purchased at a discount of 95% off, at $4.95 per user per month

Note: If you want to host your personal project on GitLab.com and you decide to make it publicly visible instead of private, you will automatically have access to GitLab's Gold features when you create a Free account. To access Gold features at the *group* level (to enable things like epics, roadmaps, merge requests, and other Gold features for groups), you'll need to upgrade to our paid Gold plan or you may consider applying for this GitLab for Open Source program.

### Who qualifies for the GitLab for Open Source program?

In order to qualify, your projects must meet the following program requirements: 
 1. **[OSI-approved open source license](https://opensource.org/licenses/category)** -- all of the code you write through this project must be published under an OSI-approved license. 
 1. **Non-profit** -- Your project must not seek to make a profit. Accepting donations to sustain your efforts is ok. 
 1. **Publicly visible.** -- Your source code must be publicly visible and publicly available.

See our [full terms](/terms/#edu-oss) regarding this program. For more questions, please see our [GitLab for Open Source FAQ](/solutions/open-source/#FAQ) or contact us at `opensource@gitlab.com`

### How to get started
 * Qualifying open source projects can start benefiting from self-hosted Ultimate and cloud-hosted Gold by completing a simple [application process](https://about.gitlab.com/solutions/open-source/program/).
 * Read more about the [Community Edition vs Enterprise Edition](https://about.gitlab.com/install/ce-or-ee/) to see what the best option is for your project. This page includes a way to download the Community Edition from there. 

### How to find help

#### GitLab Forum & Docs
The first place to look if you have questions is always the [GitLab Docs](https://docs.gitlab.com/) site. Our team keeps this updated so that it's the single source of truth, and it has answers to a wide variety of questions you may face.

If you can't find what you're looking for in our docs, consider asking in the [GitLab Forum](https://forum.gitlab.com/). There, you can interact with other members of our community and many GitLab employees. Even if you don't have questions, we encourage you to join the forum and meet other members of our community!

#### Submitting feedback: bugs, features, and more
In line with our Transparency value, GitLab has an [open roadmap](/direction/), so you can always see what's ahead in the product priorities. 

Your feedback helps us to build the best product possible. Whether it's a bug report, a feature request, or feedback to our company, here's a guide for [how to submit feedback](/submit-feedback/).

**Make sure to Thumbs Up issues.**
Our team looks at the numbder of `Thumbs Up` votes as an indicator for priority, so make sure you're using that feature on issues as a quick way to give us feedback. Writing detailed comments of your use-case and thorough issue desciptions is another way you can help make it easier for our team to understand your needs and respond to your questions. 

Just seach for relevant issues on the [GitLab product project](/gitlab-org/gitlab), and start giving us your feedback!

#### Public Migration Tracking Issue
If you're part of a large open source organization that is planning a migration to GitLab, please consider creating a public issue to track your progress. 

Public migration tracking issues help us understand which issues are blockers, urgent, important, and nice-to-have for your organization. It also has the added benefit of helping you get used to our workflow so that you can start giving us product feedback and help us improve the product together.

Follow these steps to create a public migration tracking issue:

 1. Create a [New Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/new) in the `gitlab.com/gitlab-org/gitlab` [project](/gitlab-org/gitlab)
 1. Select the `Migrations` template in the description section where it prompts you to select a template, and click on `Apply template`
 1. Add in as much information as possible. Once finished, submit it.

 Examples of public issue trackers:
  * [KDE](https://gitlab.com/gitlab-org/gitlab/-/issues/24900)
  * [Eclipse Foundation](https://gitlab.com/gitlab-org/gitlab/-/issues/195865)
  * [Freedesktop](https://gitlab.com/gitlab-org/gitlab/-/issues/217107)

#### Support add-ons and additional services
**Priority Support**
While our GitLab for Open Source program does not include paid support, members of the program receive a 95% discount on [Priority Support](/support/). You can purchase support for $4.95 per user per month by sending us an email to opensource@gitlab.com and requesting an add-on. 

**Migration Services**
If you're looking for help with your migration, our team provides a variety of services to help. Check out our [professional services](https://about.gitlab.com/services/) page for more information. 

**Additional services**
We sometimes hear requests for services we do not provide, such as providing hosting for Community Edition instances. We've partnered with others to help expand the options for our users. Check out our [Technology Partners](/partners/) to learn more.


#### Resources and additional open source programs
There are a number of organizations that offer open source programs with discounts or free services just for open source projects. Here is a list of some of the programs we know that have been useful to pair with the GitLab for Open Source program: 

 * [AWS](https://aws.amazon.com/blogs/opensource/aws-promotional-credits-open-source-projects/) - Provides promotional credits for hosting to qualifying open source projects.
 * [Packet.io](https://www.packet.com/community/open-source/) - Packet.com provides customized options for open source organizations, based on need.

## Deep Dive into the GitLab for Open Source program
Here is more information on how the GitLab for Open Source program operates. 

### OKRs (goals for current quarter)
GitLab creates [Objectives and Key Results (OKRs)](https://en.wikipedia.org/wiki/OKR) per quarter. Here are the current OKRs for the GitLab for Open Source program:
 * [Revamp OSS Program to support growth](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/epics/10)
 * [Build up OSS program relationships](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/epics/11)

### Where to find what we're working on
Epics, issue boards, and labels are used to track our work. For more information on the specific ones we use, please visit the [Community Relations project management page](/handbook/marketing/community-relations/project-management/). 

### Other resources

- Community Advocacy [workflow](/handbook/marketing/community-relations/community-advocacy/workflows/education-oss-startup/)
- GitLab for Open Source [repository](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/gitlab-oss/)

### Metrics

#### What we measure
We use the [GitLab for Open Source report](https://gitlab.my.salesforce.com/00O4M000004aCuk) on Salesforce (SFDC), along with the  [Eduoss](https://gitlab.com/gitlab-com/marketing/community-relations/community-advocacy/general/blob/master/tools/edoss/edoss.rb) script, to generate a report on: 
 * How many licenses have been issued 
 * How many seats have been issued
 * How many Gold licenses
 * How many Ultimate licenses

#### How we measure it

The steps below will generate the GitLab for Open Source Program metrics:  

1. Log into SFDC. 
1. Navigate to Reports. 
1. Open the 'GitLab for Open Source report' [SFDC report](https://gitlab.my.salesforce.com/00O4M000004aCuk)
1. Click `Run Report` to generate the report. 
1. Click `Export Details`. Choose `Unicode (UTF-8)` for the Export File Encoding and `.csv` for the Export File Format. Then click `Export` and save the file. 
1. Download the [Eduoss](https://gitlab.com/gitlab-com/marketing/community-relations/community-advocacy/general/blob/master/tools/edoss/edoss.rb) script. 
1. Open a command prompt and change to the directory where you have the script and .csv file. 
1. Run the Edoss script with the exported csv file as input to generate the final csv file. 
       `./edoss.rb -i <exported_file>.csv -o <output_file>.csv`
1. Update the [GitLab for Open Source Graphs](https://docs.google.com/spreadsheets/d/19h0Era5Bh5WoG93R2wB608n9eHonPYN4ekQrHbfrI34/edit#gid=1952747469) by importing the results of the script and updating the figures. 
1. Double-check the data for consistency. It is sometimes the case that opportunities in SFDC are mistakenly named. If that is the case, fix it on the spreadsheet and reach out to a Community Advocate to fix it on SFDC.
 * Note: The processor script can provide the data in `yaml` and `json` formats in addition to .csv. 

##### Tips and Troubleshooting

If this is your first time running the report, here's a checklist of things you'll need to make sure you have first: 
 * Make sure that the Salesforce report is ready to be processed by the script: 
    * The report should have the following columns, in this order: `Campaign Name`, `Opportunity Name`, `Close Date`, `Products Purchased`, `Account Name`, `Stage`
      * If the report does not have the correct categories, you'll need to customize the report with the following steps: 
         1. Click on "customize" in the menu right above the report
         1. Search for the missing column titles, or fields (on the left menu)
         1. Drag and drop the fields into the correct order on the report
         1. When your report has all of the correct fields in the correct order, press "Save As" (top left menu). Give your report a name and description and save it to the "Marketing Reports" folder so that others at GitLab can access your report. If you save it in your personal folder, only you will be able to see it.
    * Make sure the Salesforce report has the correct filters: `Campaign Name equals "2018_OSS"` AND `Stage equals "Closed Won"EditRemove` AND `Type equals "New Business"`
      * If the report does not have the correct filters, 
         1. Click on `Customize` 
         1. Click on `Filters > Add`: `Field Filter`
         1. Create the filter as such: `Stage :: equals :: Closed Won`. 
         1. Click `OK`
         1. Repeat these steps for any other missing filters, then "Save" your report by clicking the top left menu button.

 * Before you run the script for the first time to generate metrics report, you will need to have [Ruby](https://www.ruby-lang.org/en/documentation/installation/) installed. Check to see if you have Ruby installed by running the command: 'ruby-v'. That will tell you either the version of Ruby that you're running or else that you do not have it installed. 
 * If you run into problems running the script: 
    * You may need to make the script file executable by using the following command:
       `chmod 755 edoss.rb`
